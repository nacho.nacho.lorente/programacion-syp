#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

//El manejador del ctrl+c
void sigint_handler (int sig){
    printf("No se puede utilizar Ctrl+c\n");
}

//Contador de veces que se llama a kill
void siguser1_handler (int sig){
    int cnt = 0;
    printf("La llamada a %i de SIGUSR1 a sido enviada %i veces con kill", sig, cnt++);
}


void sigint_handler_child (int sig){
    if(getpid() == 0) //id del padre
        exit(0);
}
void spawn(){
    pid_t child_pid;
    int child_status;

    child_pid = fork();


}

int main (int argc, char *agrv[]){
    signal(SIGINT, &sigint_handler);
    signal(SIGINT, &siguser1_handler);
    signal(SIGINT, &sigint_handler_child);

    spawn();
}
